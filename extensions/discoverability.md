# Goal

The goal of this extension is to make the TRP API discoverable. With these
endpoints defined here, it is easier to make the correct requests to the
counterparty.

All requests are `GET` requests.

## Version

The version endpoint returns the version of the software running. Both the
vendor and version are returned.

### Endpoint

`/version`

### Response

```
{
  "version": "1.2.0",
  "vendor": "21Analytics"
}
```

## Uptime

Uptime is reported in seconds. This indicates the health of the service, again
facilitating debugging.

### Endpoint

`/uptime`

### Response

```
1894327
```

## Extensions

This endpoint returns a list of required and supported extensions. The lists can
be used to construct requests to the VASP.

### Endpoint

`/extensions`

### Response

```
{
  "required": ["message-signing"],
  "supported": ["extended-ivms101", "deterministic-transfer"]
}
```
