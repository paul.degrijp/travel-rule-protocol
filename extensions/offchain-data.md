# Goal

Be able to optionally add off-chain data related to the blockchain transfer.  
The recipient of this message can ignore this extension if not supported.

## Extension header name

In line with the latest specification the name would be: `offchainData`.

Used in the `api-extensions` header it would look like:

```
api-extensions: deterministic-transfer, offchain-data
```

## Extension JSON payload

The payload should be placed in the transfer notification POST request, under the reserved top level `extensions` key.  
As per the latest specification the value should be a map with a single top level key.  
This key is `offchainData`.

At minimum, the field `refId` must be defined:

- For example, a Core Banking System might require a refId for reconciliation as it is often required that the refId need to travel with the message.

## How to handle a missing reference

A client could use this extension and send a value in the `offchainData` key with `refId`.  
The server must then respond with a `400 - Bad Request` status code.   
The response body should indicate why the request was a bad one.

## Example payload

### Transfer

```
POST /assets/BTC/transactions
{
  "ivms101": ...
  "extensions": {
    "offchainData": {
      "refId": "abcd-efgh-ijkl-1234"
    }
  }
}
```